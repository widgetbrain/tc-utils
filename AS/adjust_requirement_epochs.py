import pytz
from datetime import datetime, timedelta


def bound_actual_requirements(problem, start, end):
    """
    Limits the requirements to a specific start and end epoch. If the start and end are outside the bounds,
    we create new requirements with value 0
    """
    for department in problem["requirements"].keys():
        start = int(start)
        end = int(end)
        epoch_list = [
            int(epoch)
            for epoch in list(problem["requirements"][department]["actual"].keys())
        ]
        bucket_size = epoch_list[1] - epoch_list[0]
        old_start = epoch_list[0]
        old_end = epoch_list[-1]
        if old_start < start:
            epoch_list = [epoch for epoch in epoch_list if epoch >= start]
        elif old_start > start:
            epoch_list = [
                epoch for epoch in range(start, old_start, bucket_size)
            ] + epoch_list
        if old_end > end:
            epoch_list = [epoch for epoch in epoch_list if epoch < end]
        elif old_end < end:
            epoch_list = epoch_list + [
                epoch for epoch in range(old_end, end, bucket_size)
            ]

        new_requirements = {}
        for epoch in epoch_list:
            if f"{epoch}" in problem["requirements"][department]["actual"]:
                new_requirements[f"{epoch}"] = problem["requirements"][department][
                    "actual"
                ][f"{epoch}"]
            else:
                new_requirements[f"{epoch}"] = 0
        problem["requirements"][department]["actual"] = new_requirements


def get_first_days(problem, forward, n):
    """
    Returns a payload consisting of the first n days. If 'forward' is set to true, we start with the first day that
    follows the first epoch in the payload. If not, we start with the day of the first epoch .
    """
    first_epoch = [
        int(epoch)
        for epoch in list(list(problem["requirements"].values())[0]["actual"].keys())
    ][0]
    tz = (
        pytz.timezone(problem["settings"]["time_zone"])
        if "time_zone" in problem["settings"]
        else pytz.utc
    )
    dt = datetime.fromtimestamp(first_epoch, tz=tz)
    if not forward:
        start = datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo).timestamp()
        end = (
            datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo) + timedelta(days=n)
        ).timestamp()
        bound_actual_requirements(problem, start, end)
    else:
        start = (
            datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo) + timedelta(days=1)
        ).timestamp()
        end = (
            datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo)
            + timedelta(days=n + 1)
        ).timestamp()
        bound_actual_requirements(problem, start, end)


def get_first_weeks(problem, forward, n):
    """
    Returns a payload consisting of the first n weeks. If 'forward' is set to true, we start with the first full week
    that follows the first epoch in the payload. If not, we start with the full week of the first epoch .
    """
    first_epoch = [
        int(epoch)
        for epoch in list(list(problem["requirements"].values())[0]["actual"].keys())
    ][0]
    tz = (
        pytz.timezone(problem["settings"]["time_zone"])
        if "time_zone" in problem["settings"]
        else pytz.utc
    )
    dt = datetime.fromtimestamp(first_epoch, tz=tz)
    day_of_week = dt.weekday()
    if not forward:
        start = (
            datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo)
            - timedelta(days=day_of_week)
        ).timestamp()
        end = (
            datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo)
            + timedelta(days=7 * n - day_of_week)
        ).timestamp()
        bound_actual_requirements(problem, start, end)
    else:
        start = (
            datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo)
            + timedelta(days=7 - day_of_week)
        ).timestamp()
        end = (
            datetime(dt.year, dt.month, dt.day, tzinfo=dt.tzinfo)
            + timedelta(days=7 * (n + 1) - day_of_week)
        ).timestamp()
        bound_actual_requirements(problem, start, end)
