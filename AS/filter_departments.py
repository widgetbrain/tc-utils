def filter_departments(problem, departments):
    """Reduces a payload to only the departments specified in the 'departments' argument"""
    if isinstance(departments, str):
        departments = [departments]

    problem["departments"] = [department for department in problem["departments"] if department["id"] in departments]
    problem["requirements"] = {key: value for key, value in problem["requirements"].items() if f"{key}" in departments}
    problem["users"] = [user for user in problem["users"] if any(department_id in departments for department_id in user["department_ids"])]
    problem["shift_types"] = [st for st in problem["shift_type"] if not "department_ids" in st or st["department_ids"] == [] or any(department_id in departments for department_id in st["department_ids"])]

    if isinstance(problem["settings"], list):
        problem["settings"] = [setting for setting in problem["settings"] if any(department_id in departments for department_id in setting["department_ids"])]
