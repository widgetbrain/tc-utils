from alfa_sdk.common.session import Session


def invoke_algorithm(
    client_id,
    client_secret,
    algorithm_id,
    environment,
    problem,
    return_holding_response=False,
    include_details=False,
    can_buffer=False,
):
    """
    Invokes an algorithm
    """
    credentials = {"client_id": client_id, "client_secret": client_secret}
    session = Session(credentials)
    res = session.invoke_algorithm(
        algorithm_id=algorithm_id,
        environment=environment,
        problem=problem,
        return_holding_response=return_holding_response,
        include_details=include_details,
        can_buffer=can_buffer,
    )
