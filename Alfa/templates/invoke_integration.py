from alfa_sdk.common.session import Session


def invoke_integration(
    client_id, client_secret, integration_id, environment, function_name, problem
):
    """
    Invokes an integration
    """
    credentials = {"client_id": client_id, "client_secret": client_secret}
    session = Session(credentials)
    res = session.invoke_integration(
        integration_id=integration_id,
        environment=environment,
        problem=problem,
        function_name=function_name,
    )
