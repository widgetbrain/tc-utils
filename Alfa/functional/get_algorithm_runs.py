from utils import get_date_time, get_date_time_before_date
from Alfa.alfa import alfaHandler

"""Credentials"""
client_id = ""
client_secret = ""

"""Settings"""
to = ""  # leave empty if you want the current time, otherwise it accepts a datetime object
days_before_to = 7
team_id = ""

if len(to) == 0:
    to = get_date_time()
_from = get_date_time_before_date(to, days_before_to)

alfa_handler = alfaHandler(client_id, client_secret)
all_runs = alfa_handler.fetch_algorithm_runs(_from, to, team_id)

for run in all_runs:
    run_data = alfa_handler.fetch_algorithm_run_data(run["algorithmRunId"])
