from datetime import datetime, timedelta


def get_date_time():
    return datetime.now()


def get_date_time_before_date(date, days):
    return date - timedelta(days=days)
