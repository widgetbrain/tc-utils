from utils import get_date_time, get_date_time_before_date
from Alfa.alfa import alfaHandler

"""Credentials"""
client_id = ""
client_secret = ""

"""Settings"""
to = ""  # leave empty if you want it now, otherwise, it accepts a datetime object
days_before_to = 7
team_id = "team id"
customer_tag = ""
function_name = "function name"

"""Process"""
if len(to) == 0:
    to = get_date_time()
_from = get_date_time_before_date(to, days_before_to)

alfa_handler = alfaHandler(client_id, client_secret)
all_runs = alfa_handler.fetch_integration_runs(_from, to, team_id)

runs_to_analyze = [run for run in all_runs if run['functionName'] == function_name]
results = []

for run in runs_to_analyze:
    run_data = alfa_handler.fetch_integration_run_data(run["runId"])
    if run_data['payload']['customerTag'] == customer_tag:
        results.append(run_data)



