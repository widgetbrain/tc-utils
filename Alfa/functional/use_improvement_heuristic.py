from Alfa.alfa import alfaHandler
from copy import deepcopy

"""Authorization"""
client_id = "Your Client Id"
client_secret = "Your Client Secret"

"""Settings"""
original_payload = {}
algorithm_id = "AA algorithm id"
environment = "AA environment to be used"
first_run_settings = {
    "runtime": 80,
    "run_shifting_window": True,
    "shifting_window_day_size": 1,
    "fix_window_day_size": 1,
    "include_shifts_days": 7,
    "fix_drop_ratio": 0,
}
improvement_settings = {"improve_time_limit": 450, "runtime": 5}

"""Main"""
alfa_handler = alfaHandler(client_id, client_secret)

first_run_payload = deepcopy(original_payload)
for setting, value in first_run_settings.items():
    first_run_payload["settings"][setting] = value

first_result = alfa_handler.invoke_algorithm(
    algorithm_id=algorithm_id, environment=environment, problem=first_run_payload
)

for original_shift in original_payload["shifts"]:
    for assigned_shift in first_result["shifts"]:
        if (
            str(original_shift["id"]) == str(assigned_shift["shift_id"])
            and not original_shift["is_fixed"]
        ):
            original_shift["user_id"] = assigned_shift["user_id"]
            break

improvement_payload = deepcopy(original_payload)
for setting, value in improvement_settings.items():
    improvement_payload["settings"][setting] = value

final_result = alfa_handler.invoke_algorithm(
    algorithm_id=algorithm_id, environment=environment, problem=improvement_payload
)
