from alfa_sdk.common.session import Session


class alfaHandler:
    """
    Handles all requests to Alfa.
    """

    def __init__(self, client_id, client_secret):
        credentials = {"client_id": client_id, "client_secret": client_secret}
        self.session = Session(credentials)

    def invoke_integration(self, integration_id, environment, problem, function_name):
        return self.invoke_integration(
            integration_id=integration_id,
            environment=environment,
            problem=problem,
            function_name=function_name,
        )

    def invoke_algorithm(
        self,
        algorithm_id,
        environment,
        problem,
        return_holding_response=False,
        include_details=False,
        can_buffer=False,
    ):
        """
        Invokes an algorithm
        """
        return self.session.invoke_algorithm(
            algorithm_id=algorithm_id,
            environment=environment,
            problem=problem,
            return_holding_response=return_holding_response,
            include_details=include_details,
            can_buffer=can_buffer,
        )

    def fetch_algorithm_runs(self, _from, to, team_id, datapoints=1000):
        params = {"from": _from, "to": to, "teamId": team_id, "datapoints": datapoints, "appId": 30}
        return self.session.request(
            service="baas",
            path="/api/RunLogs/getSingleRunLogs",
            params=params,
            method="get"
        )

    def fetch_algorithm_run_data(self, run_id):
        return self.session.request(
            service="baas",
            path="/api/algorithmRuns/" + run_id,
            method="get"
        )

    def fetch_integration_runs(self, _from, to, team_id):
        params = {"from": _from, "to": to, "teamId": team_id}
        return self.session.request(
            service="ais",
            path="/api/Invocations",
            params=params,
            method="get"
        )

    def fetch_integration_run_data(self, run_id):
        return self.session.request(
            service="ais",
            path="/api/Invocations/" + run_id,
            method="get"
        )
