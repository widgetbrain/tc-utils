from alfa_sdk.common.session import Session
import json


class Pythia:
    """
    Handles all requests to pythia.
    """

    def __init__(self, client_id=None, client_secret=None):
        credentials = {"client_id": client_id, "client_secret": client_secret}
        self.session = Session() if client_id is None or client_secret is None else Session(credentials)

    def get_customer_hierarchy(self):
        return self.session.request(
            method="GET",
            service="pythia",
            path="/api/Organisations/getCustomerUnitHierarchy/",
        )

    def upsert_shift_fill_rule_configuration(
        self, name, customer_id, allowed_units=[], id=None, is_active=False
    ):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/RuleConfigurations/upsertEntity",
            data={
                "id": id,
                "name": name,
                "customerId": customer_id,
                "allowedUnits": allowed_units,
                "isActive": is_active,
            },
        )

    def upsert_attribute(
        self,
        name,
        tag,
        unit_id,
        customer_id,
        external_database_id,
        is_calculated=False,
        bucket_size=900000,
        register_meta_unit=False,
        configure_demand_forecasting_algorithm=False,
        configure_anomaly_detection_algorithm=False,
    ):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/Attributes/upsertAttribute",
            data={
                "name": name,
                "tag": tag,
                "unitId": unit_id,
                "customerId": customer_id,
                "externalDatabaseId": external_database_id,
                "isCalculated": is_calculated,
                "bucketSize": bucket_size,
                "registerMetaUnit": register_meta_unit,
                "configureDemandForecastingAlgorithm": configure_demand_forecasting_algorithm,
                "configureAnomalyDetectionAlgorithm": configure_anomaly_detection_algorithm
            },
        )

    def upsert_unit(
        self,
        name,
        tag,
        unit_id,
        customer_id,
        unit_parameters={},
    ):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/Units/upsertUnit",
            data={
                "name": name,
                "tag": tag,
                "id": unit_id,
                "customerId": customer_id,
                "unitParameters": json.dumps(unit_parameters),
            },
        )

    def get_opening_hour_configurations(self):
        return self.session.request(
            method="GET",
            service="pythia",
            path="/api/OpeningHourConfigurations/getList",
            data={},
        )

    def delete_opening_hour_configuration(
        self,
        _id,
    ):
        return self.session.request(
            method="GET",
            service="pythia",
            path=f"/api/OpeningHourConfigurations/deleteEntity",
            data={"id": _id},
        )

    def get_min_max_configurations(self):
        return self.session.request(
            method="GET",
            service="pythia",
            path="/api/MinMaxConfigurations/getList",
            data={},
        )

    def upsert_min_max_configuration(
        self,
        name,
        type,
        customer_id,
        allowed_units,
        is_active,
    ):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/MinMaxConfigurations/upsertEntity",
            data={
                "name": name,
                "type": type,
                "customerId": customer_id,
                "allowedUnits": allowed_units,
                "isActive": is_active,
            },
        )

    def delete_min_max_configuration(
        self,
        id,
    ):
        return self.session.request(
            method="DELETE",
            service="pythia",
            path=f"/api/MinMaxConfigurations/deleteEntity/{id}",
            data={},
        )

    def get_attribute_calculation_configurations(self):
        return self.session.request(
            method="GET",
            service="pythia",
            path="/api/AttributeCalculationConfigs/getList",
            data={},
        )

    def upsert_attribute_calculation_configuration(self, name, algorithm_environment_id, attribute_id, auto_generate=True):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/AttributeCalculationConfigs/upsertEntity",
            data={
                "name": name,
                "algorithmEnvironmentId": algorithm_environment_id,
                "attributeId": attribute_id,
                "autoGenerate": auto_generate
            },
        )

    def upsert_attribute_calculation_configuration_type(self, attribute_calculation_config_id, _type):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/AttributeCalculationConfigTypes/upsertEntity",
            data={
                "attributeCalculationConfigId": attribute_calculation_config_id,
                "type": _type
            },
        )

    def upsert_attribute_calculation_configuration_parameters(self, attribute_calculation_config_type_id, days_of_week, weeks_of_month, input_attribute_id, _min, _max, min_boundary):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/AttributeCalculationConfigParameters/upsertEntity",
            data={
                "attributeCalculationConfigTypeId": attribute_calculation_config_type_id,
                "daysOfWeek": days_of_week,
                "weeksOfMonth": weeks_of_month,
                "inputAttributeId": input_attribute_id,
                "min": _min,
                "max": _max,
                "minBoundary": min_boundary
            },
        )

    def upsert_min_max_definition(
        self, name, start, end, value, weekdays, configuration_id
    ):
        return self.session.request(
            method="POST",
            service="pythia",
            path="/api/MinMaxDefinitions/upsertEntity",
            data={
                "name": name,
                "start": start,
                "end": end,
                "value": value,
                "weekdays": weekdays,
                "configurationId": configuration_id,
            },
        )

    def delete_unit(self, unit_id):
        return self.session.request(
            method="DELETE",
            service="pythia",
            path="/api/Units/deleteUnit",
            data={"id": unit_id},
        )
