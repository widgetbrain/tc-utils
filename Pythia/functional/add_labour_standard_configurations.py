from Pythia.pythia import Pythia

"""Settings"""
add_unit_name_to_configuration = True
customer_tag = ""
labour_standard_configs = []

"""Main"""
pythia_handler = Pythia()
hierarchy = pythia_handler.get_customer_hierarchy()

for customer in hierarchy:
    if customer["tag"] == customer_tag:
        customer_id = customer["id"]
        #TODO: add loop using the api calls
