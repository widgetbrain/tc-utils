from Pythia.pythia import Pythia


"""Settings"""
customer_tags = ["swarovski_north_america"]

"""Main"""
pythia_handler = Pythia()
hierarchy = pythia_handler.get_customer_hierarchy()

for customer in hierarchy:
    if customer["tag"] in customer_tags:
        customer_id = customer["id"]
        all_configs = pythia_handler.get_opening_hour_configurations()
        customer_configs = [c for c in all_configs if c["customerId"] == customer_id]
        for customer_config in customer_configs:
            pythia_handler.delete_opening_hour_configuration(customer_config['id'])
