from Pythia.pythia import Pythia
from utils import attribute_exists

"""Authorization"""
client_id = "Your Client Id"
client_secret = "Your Client Secret"

"""Settings"""
customer_tags = ["Customer Tags"]
check_if_attribute_already_exists = True
attribute_settings = [
    {
        "name": "headcounts",
        "tag": "headcounts",
        "externalDatabaseId": "headcounts",
        "isCalculated": True,
    }
]

"""Main"""
pythia_handler = Pythia(client_id, client_secret)
hierarchy = pythia_handler.get_customer_hierarchy()

for customer in hierarchy:
    if customer["tag"] in customer_tags:
        customer_id = customer["id"]
        for unit in customer["units"]:
            unit_id = unit["id"]
            for attribute_setting in attribute_settings:
                if not check_if_attribute_already_exists or not attribute_exists(
                    unit, attribute_setting
                ):
                    pythia_handler.upsert_attribute(
                        name=attribute_setting["name"],
                        tag=attribute_setting["tag"],
                        unit_id=unit_id,
                        customer_id=customer_id,
                        external_database_id=attribute_setting["externalDatabaseId"],
                        is_calculated=attribute_setting["isCalculated"],
                    )
