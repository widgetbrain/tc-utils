from Pythia.pythia import Pythia
from utils import deep_update

"""Authorization"""
client_id = "Your Client Id"
client_secret = "Your Client Secret"

"""Settings"""
customer_tag = ""
additional_parameters = {
    "auth": {"clientId": "", "clientSecret": ""},
    "requirements": [
        {
            "attributeId": "external database id of the attribute",
            "departmentId": "integer value of the quinyx shift type id attached to the headcount",
            "name": "headcount name",
        }
    ],
}

"""Main"""
pythia_handler = Pythia(client_id, client_secret)
hierarchy = pythia_handler.get_customer_hierarchy()

for customer in hierarchy:
    if customer["tag"] == customer_tag:
        customer_id = customer["id"]
        for unit in customer["units"]:
            unit_id = unit["id"]
            new_unit_parameters = deep_update(
                unit["unitParameters"], additional_parameters
            )
            pythia_handler.upsert_unit(
                name=unit["name"],
                tag=unit["tag"],
                unit_id=unit_id,
                customer_id=customer_id,
                unit_parameters=new_unit_parameters,
            )
