from Pythia.pythia import Pythia

"""Authorization"""
client_id = "Your client Id"
client_secret = "Your client secret"

"""Settings"""
customer_tags = []
unit_names = ["Unit name to delete"]
only_delete_units_without_external_id = True

"""Main"""
pythia_handler = Pythia(client_id, client_secret)
hierarchy = pythia_handler.get_customer_hierarchy()

for customer in hierarchy:
    if customer["tag"] in customer_tags:
        customer_id = customer["id"]
        for unit in customer["units"]:
            if (len(unit_names) == 0 or unit["name"] in unit_names) and (
                not only_delete_units_without_external_id
                or unit["externalDatabaseId"] is None
            ):
                pythia_handler.delete_unit(unit_id=unit["id"])
