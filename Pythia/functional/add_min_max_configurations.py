from Pythia.pythia import Pythia

"""Authorization"""
client_id = "Your Client Id"
client_secret = "Your Client Secret"

"""Settings"""
add_unit_name_to_requirements = True
delete_all_existing_configs = True
customer_tag = ""
min_max_configs = [
    {
        "type": "minimum",
        "name": "Minimum requirements",
        "is_active": True,
        "min_max_definitions": [
            {
                "name": "Mon-Thu",
                "start": 435,
                "end": 1395,
                "value": 5,
                "weekdays": [0, 1, 2, 3],
            },
            {
                "name": "Fri-Sat",
                "start": 435,
                "end": 1440,
                "value": 5,
                "weekdays": [4, 5],
            },
        ],
    }
]

"""Main"""
pythia_handler = Pythia(client_id, client_secret)
hierarchy = pythia_handler.get_customer_hierarchy()

for customer in hierarchy:
    if customer["tag"] == customer_tag:
        customer_id = customer["id"]
        if delete_all_existing_configs:
            all_configs = pythia_handler.get_min_max_configurations()
            for config in all_configs:
                if config["customerId"] == str(customer_id):
                    pythia_handler.delete_min_max_configuration(config["id"])
        for unit in customer["units"]:
            unit_id = unit["id"]
            for min_max_config in min_max_configs:
                name = min_max_config["name"]
                if add_unit_name_to_requirements:
                    name += " " + unit["name"]
                added_config = pythia_handler.upsert_min_max_configuration(
                    name=name,
                    type=min_max_config["type"],
                    customer_id=customer_id,
                    allowed_units=f"{unit_id}",
                    is_active=min_max_config.get("is_active", True),
                )
                for min_max_definition in min_max_config["min_max_definitions"]:
                    pythia_handler.upsert_min_max_definition(
                        name=min_max_definition["name"],
                        start=min_max_definition["start"],
                        end=min_max_definition["end"],
                        value=min_max_definition["value"],
                        weekdays=min_max_definition["weekdays"],
                        configuration_id=added_config["id"],
                    )
