import collections


def attribute_exists(unit, attribute_setting):
    for attribute in unit["attributes"]:
        if all(
            attribute_setting[key] == attribute[key] for key in attribute_setting.keys()
        ):
            return True
    return False


def deep_update(source, overrides):
    """
    Updates a nested dictionary (source) with another nested dictionary (overrides)
    """
    for key, value in overrides.items():
        if isinstance(value, collections.Mapping) and value:
            returned = deep_update(source.get(key, {}), value)
            source[key] = returned
        else:
            source[key] = overrides[key]
    return source
