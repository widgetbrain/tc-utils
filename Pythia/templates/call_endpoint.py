from alfa_sdk.common.session import Session


def call_pythia_endpoint(client_id, client_secret, method, url, data):
    """
    Call a pythia endpoint, using the following arguments:

    client_id: YOUR CLIENT ID HERE
    client_secret: YOUR CLIENT SECRET HERE
    method: Method of the request, e.g. 'GET'
    url: request URL, as can be found on the API explorer. For example, '/api/Organisations/getCustomerUnitHierarchy/'
    """
    credentials = {"client_id": client_id, "client_secret": client_secret}
    session = Session(credentials)
    res = session.request(method=method, service="pythia", path=url, data=data)
    return res
